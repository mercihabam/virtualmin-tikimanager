# Virtualmin Tiki Manager

This Virtualmin plugin allows users to manage Tiki Wiki CMS Groupware instances
within Virtualmin Virtual Servers. The plugin code wraps Tiki Manager command
line calls from Virtualmin web interface.

## Install
It is installed as part of the standard WikiSuite installer. Installing on Virtualmin (outside of WikiSuite) is not recommended / supported yet. If you are a developer, you'll be able to figure it out.

## Documentation
https://wikisuite.org/Virtualmin-Tiki-Manager

## CLI usage
### Tiki install
```
virtualmin tiki-install --domain test1.example.com --branch 22.x --password tikiwiki
```

## Remote CLI usage
### Tiki install
```sh
vmin_domain=test1.example.com
tiki_branch=22.x
tiki_password=tikiwiki

vmin_user=fabio
vmin_pass=********
vmin_host=example.com
vmin_port=10000

url="https://${vmin_host}:${vmin_port}/virtual-server/remote.cgi?json=1&multiline"
url+="&program=tiki-install"
url+="&domain=${vmin_domain}"
url+="&branch=${tiki_branch}"
url+="&password=${tiki_password}"

curl --user "${vmin_user}:${vmin_pass}" "${url}"
```
