#!/usr/bin/perl
use strict;
use warnings;
use Cwd qw(abs_path);
use File::Basename;

our (%access, %text, %in, %config);
our ($module_name);

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}
if (! $d) {
  &error("Domain not found.");
}

# TODO: abstract to use with tiki-edit-ini.cgi
chdir($d->{'home'});
my $config = &tikimanager_tiki_get_config($d) || &error("Can't open local.php");
my ($info) = &tikimanager_tiki_info($d);
my $branch = int($info->{'branch'});
my $is_master = $info->{'branch'} == 'master';
my $file_path = &tikimanager_get_pref_file_path($d);
my $file = ($branch < 21 || $info->{'branch'} != 'master') ? $file_path->{'prefs_ini'} : $file_path->{'prefs_ini_php'};
if (-e $config->{'system_configuration_file'}) {
  $file = $config->{'system_configuration_file'};
}
my $identifier = 'tikiwiki';

eval_as_unix_user($d->{'user'}, sub { &useradmin::mkdir_if_needed(dirname($file)); });

my $data = $in{'data'};
$data =~ s/\r\n/\n/g;

my $php_code = &tikimanager_get_ini_php_code();
if (($branch > 20 || $is_master )) {
  $data = $data =~ s/\Q$php_code\E//r;
}

if ($data =~ m/ *\[([^\]]+) *\]/) {
  $identifier = $1;
}
else {
  $data = "\n[$identifier]\n$data";
}
# for version greater than 21 save to 'pres.ini.php' and add php code to protect read data.
if (($branch > 20 || $is_master )) {
  $data = $php_code . $data;
}

my $SAVE;
&open_tempfile($SAVE, ">" . abs_path($file)) || &error("Can't save tiki ini file: " . &html_escape("$!"));
&print_tempfile($SAVE, $data);
&close_tempfile($SAVE);
system('chown -R -h ' . $d->{'uid'} . ':' . $d->{'gid'} .' ' . "$file");

$config->{'system_configuration_file'} = $file;
$config->{'system_configuration_identifier'} = $identifier;

&tikimanager_tiki_save_config($d, $config) || &error("local.php was not saved or corrupted");

if ($in{'save_close'}) {
  &redirect("index.cgi?dom=$d->{'id'}");
} else {
  &redirect("tiki-edit-ini.cgi?dom=$d->{'id'}");
}